function botoes(balao_da_mensagem, mensagem){
    let botao_editar = document.createElement("button");
    botao_editar.classList.add("editar");
    balao_da_mensagem.appendChild(botao_editar);
    botao_editar.innerHTML = "Editar";

    let botao_excluir = document.createElement("button");
    botao_excluir.classList.add("excluir");
    balao_da_mensagem.appendChild(botao_excluir);
    botao_excluir.innerHTML = "Excluir";
    
    botao_excluir.addEventListener("click", () => {
        botao_excluir.remove();
        botao_editar.remove();
        mensagem.remove();
    })
}

function enviar(){
    let msg_escrita_usr = document.querySelector("#escreve_aqui").value;
    if (msg_escrita_usr == ""){
        alert("Por favor adicione alguma mensagem!")
    } else {
        let mensagem = document.createElement("div");
        let balao_da_mensagem = document.getElementById("mensagens");
        mensagem.innerText = msg_escrita_usr;
        balao_da_mensagem.appendChild(mensagem);
        mensagem.classList.add("mensagem");
    
        botoes(balao_da_mensagem, mensagem);
        }
    }


let botao_enviar = document.querySelector("#enviar");
botao_enviar.addEventListener("click", enviar)
